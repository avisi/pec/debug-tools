FROM registry.gitlab.com/avisi/base/centos:7

USER root
RUN yum install -y telnet bind-utils tcpdump nmap net-tools

COPY install-tools.sh /install-tools.sh
RUN /install-tools.sh
